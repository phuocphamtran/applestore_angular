import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NodeProductComponent } from './node-product.component';

describe('NodeProductComponent', () => {
  let component: NodeProductComponent;
  let fixture: ComponentFixture<NodeProductComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NodeProductComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
