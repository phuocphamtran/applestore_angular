import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InsertNdodeComponent } from './insert-ndode.component';

describe('InsertNdodeComponent', () => {
  let component: InsertNdodeComponent;
  let fixture: ComponentFixture<InsertNdodeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InsertNdodeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InsertNdodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
