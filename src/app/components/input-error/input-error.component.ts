import { Component, Input, OnInit } from "@angular/core";
import { AbstractControl, FormControl } from "@angular/forms";

@Component({
  selector: "[app-input-error]",
  templateUrl: "./input-error.component.html",
  styleUrls: ["./input-error.component.css"],
})
export class InputErrorComponent implements OnInit {
  @Input("errors") _errors: any;
  @Input("field") _field?: string;
  @Input("label") label?: string;

  constructor() {}

  ngOnInit() {}

  getMessage(type: string, error: any): string {
    switch (type) {
      case "required":
        return `${this.label} is required`;
      case "email":
        return `${this.label} format is invalid`;
      case "minlength":
        return `${this.label} length must greater than ${error.requiredLength}`;
      case "between":
        return `${this.label} must between ${error.min} and ${error.max}`;
      case "notsame":
        return `Password not match`;
      case "unique":
        return `${this.label} has already be taken`;
      case "invalidCredentials":
        return `Your credentials is invalid!`;
      default:
        return `${this.label} is invalid`;
    }
  }

  getError(): string {
    if (this._errors) {
      for (let key in this._errors) {
        return this.getMessage(key, this._errors[key]);
      }
    }
    return "";
  }
}
