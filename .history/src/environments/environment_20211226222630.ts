// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyB91A_2sUkm9cGIrwqxle1TLcNo268wApM",
    authDomain: "appleshop-angular.firebaseapp.com",
    projectId: "appleshop-angular",
    storageBucket: "appleshop-angular.appspot.com",
    messagingSenderId: "202389999009",
    appId: "1:202389999009:web:6f76a69f75132937818fe0",
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
