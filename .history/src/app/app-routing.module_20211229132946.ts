import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppleListComponent } from "./apple-list/apple-list.component";
import { LoginLayoutComponent } from "./components/login-layout/login-layout.component";
import { LoginComponent } from "./components/login/login.component";
import { MainLayoutComponent } from "./components/main-layout/main-layout.component";
import { AuthGuard } from "./guards/auth.guard";
import { NodeProductComponent } from "./node-product/node-product.component";
import { ProductFormComponent } from "./product-form/product-form.component";
import { ProductListComponent } from "./product-list/product-list.component";
import { UserFormComponent } from "./user-form/user-form.component";
import { UserListComponent } from "./user-list/user-list.component";

const routes: Routes = [
  { path: "login", component: LoginLayoutComponent },
  {
    path: "admin",
    component: MainLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      { path: "product", component: ProductListComponent },
      { path: "product/form/:id", component: ProductFormComponent },
      { path: "product/form", component: ProductFormComponent },
      { path: "user", component: UserListComponent },
      { path: "user/form/:id", component: UserFormComponent },
      { path: "user/form", component: UserFormComponent },
      { path: "nodeserver", component: AppleListComponent },
      { path: "nodeserver/insert", component: AppleListComponent },
    ],
  },
  { path: "**", redirectTo: "admin" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
