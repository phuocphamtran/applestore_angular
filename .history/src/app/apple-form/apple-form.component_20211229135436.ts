import { Component, OnInit } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { NotifierService } from "angular-notifier";
import { first } from "rxjs";
import { Product } from "../models/products";
@Component({
  selector: "app-apple-form",
  templateUrl: "./apple-form.component.html",
  styleUrls: ["./apple-form.component.css"],
})
export class AppleFormComponent implements OnInit {
  _myForm!: FormGroup;
  constructor(private _formBuilder: FormBuilder) {
    this._myForm = this._formBuilder.group({
      name: ["", Validators.required],
      color: ["", Validators.required],
      price: ["", Validators.required],
      status: ["", Validators.required],
      storage: ["", Validators.required],

      // image: ['', Validators.required]
    });
  }
  onSubmit() {
    let product = new Product();
    product.name = this._myForm.controls.name.value;
    product.color = this._myForm.controls.color.value;
    product.price = this._myForm.controls.price.value;
    product.status = this._myForm.controls.status.value;
    product.storage = this._myForm.controls.storage.value;
    // product.image = this.insertForm.controls.image.value;
    this.service.insertProduct(product).subscribe((data) => console.log(data));
    // this.router.navigate(['/admin/product']);
    location.href = "/admin/product";
  }
  ngOnInit(): void {}
}
