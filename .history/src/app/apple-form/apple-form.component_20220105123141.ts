import { Component, OnInit } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { NotifierService } from "angular-notifier";
import { first } from "rxjs";
import { Product } from "../models/products";
import { ProductsServicesService } from "../services/products-services.service";

@Component({
  selector: "app-apple-form",
  templateUrl: "./apple-form.component.html",
  styleUrls: ["./apple-form.component.css"],
})
export class AppleFormComponent implements OnInit {
  //
  item?: any;
  //
  _myForm: FormGroup;
  constructor(
    private _formBuilder: FormBuilder,
    private notifier: NotifierService,
    private service: ProductsServicesService,
    private router: Router
  ) {
    this._myForm = this._formBuilder.group({
      name: ["", Validators.required],
      color: ["", Validators.required],
      price: ["", Validators.required],
      status: ["", Validators.required],
      storage: ["", Validators.required],
    });
  }
  // onSubmit() {
  //   let product = new Product();
  //   product = this._myForm.value;

  //   console.log(product);
  //   this.service
  //     .insertProduct(product)
  //     .subscribe((data: any) => console.log(data));
  //   this.notifier.notify("success", "Add success");
  //   this.router.navigate(["/admin/nodeserver"]);
  // }

  onSubmit() {
    let product = new Product();
    product = this._myForm.value;
    if (!!this.item?.id) {
      this.service
        .updateProduct(product)
        .subscribe((data: any) => console.log(data));
      this.notifier.notify("success", "update success");
      this.router.navigate(["/admin/nodeserver"]);
    } else {
      // console.log(product);
      this.service
        .insertProduct(product)
        .subscribe((data: any) => console.log(data));
      this.notifier.notify("success", "Add success");
      this.router.navigate(["/admin/nodeserver"]);
    }
    // console.log(product);
    this.service
      .insertProduct(product)
      .subscribe((data: any) => console.log(data));
    this.notifier.notify("success", "Add success");
    this.router.navigate(["/admin/nodeserver"]);
  }
  ngOnInit(): void {
    console.log(`haha`);
  }
}
