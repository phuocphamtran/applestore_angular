import { Component, OnInit } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { NotifierService } from "angular-notifier";
import { first } from "rxjs";
import { Product } from "../models/products";
import { ProductsServicesService } from "../services/products-services.service";

@Component({
  selector: "app-apple-form",
  templateUrl: "./apple-form.component.html",
  styleUrls: ["./apple-form.component.css"],
})
export class AppleFormComponent implements OnInit {
  _myForm: FormGroup;
  constructor(
    private _formBuilder: FormBuilder,
    private notifier: NotifierService,
    private service: ProductsServicesService
  ) {
    this._myForm = this._formBuilder.group({
      name: ["", Validators.required],
      color: ["", Validators.required],
      price: ["", Validators.required],
      status: ["", Validators.required],
      storage: ["", Validators.required],
    });
  }
  onSubmit() {
    let product = new Product();
    product = this._myForm.value;
    // product.color = this._myForm.controls.color.value;
    // product.price = this._myForm.controls.price.value;
    // product.status = this._myForm.controls.status.value;
    // product.storage = this._myForm.controls.storage.value;
    // product.image = this.insertForm.controls.image.value;
    // // this.router.navigate(['/admin/product']);
    // location.href = "/admin/product";
    console.log(product);
    this.service
      .insertProduct(product)
      .subscribe((data: any) => console.log(data));
    this.notifier.notify("success", "Add success");
  }
  ngOnInit(): void {
    console.log(`haha`);
  }
}
