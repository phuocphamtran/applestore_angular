export class Product {
  name?: string;
  color?: string;
  storage?: number;
  status?: string;
  price?: number;
}
