export class Product {
  id?: string;
  name?: string;
  color?: string;
  storage?: number;
  status?: string;
  price?: number;
}
