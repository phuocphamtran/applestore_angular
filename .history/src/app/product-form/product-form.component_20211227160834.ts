import { Component, OnInit } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { ActivatedRoute } from "@angular/router";
import { first } from "rxjs";
import { Product } from "../product-list/product-list.component";

@Component({
  selector: "app-product-form",
  templateUrl: "./product-form.component.html",
  styleUrls: ["./product-form.component.css"],
})
export class ProductFormComponent implements OnInit {
  item?: Product;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private readonly afs: AngularFirestore
  ) {}

  ngOnInit(): void {
    this._activatedRoute.params.subscribe((params) => {
      const { id } = params;
      if (!!id) {
        //edit
        this.afs
          .collection("products")
          .doc(id)
          .valueChanges()
          .pipe(first())
          .subscribe((item) => {
            item = item;
            console.log("item", item);
          });
      } else {
        // add
        alert("add");
      }
    });
  }
}
