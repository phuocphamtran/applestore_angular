import { Component, OnInit } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { NotifierService } from "angular-notifier";
import { first } from "rxjs";
import { Product } from "../product-list/product-list.component";

@Component({
  selector: "app-product-form",
  templateUrl: "./product-form.component.html",
  styleUrls: ["./product-form.component.css"],
})
export class ProductFormComponent implements OnInit {
  item?: any;
  _myForm!: FormGroup;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private afs: AngularFirestore,
    private _formBuilder: FormBuilder,
    private notifier: NotifierService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this._activatedRoute.params.subscribe((params) => {
      const { id } = params;
      if (!!id) {
        //edit
        this.afs
          .collection("Products")
          .doc(id)
          .valueChanges({ idField: "id" })
          .pipe(first())
          .subscribe((item) => {
            this.item = item;
            this.initForm();

            // console.log("item", item);
          });
      } else {
        // add
        this.initForm();
      }
    });
  }

  public onSubmit() {
    if (this._myForm.valid) {
      const updateData = this._myForm.value;
      if (!!this.item?.id) {
        this.afs
          .collection("Products")
          .doc(this.item!.id)
          .update(updateData)
          .then((_) => {
            this.notifier.notify("success", "Update success");
            this._myForm.reset();
            this.router.navigate(["/admin/product"]);
          })
          .catch((_) => {
            alert("ERRR");
          });
      } else {
        this.afs
          .collection("Products")
          .add(updateData)
          .then((_) => {
            this.notifier.notify("success", "Add success");
            this._myForm.reset();
            this.router.navigate(["/admin/product"]);
          });
      }
    }
  }

  public isSubmitDisabled() {
    return !this._myForm.valid;
  }

  private initForm() {
    this._myForm = this._formBuilder.group({
      color: [this.item?.color || "", Validators.required],
      storage: [this.item?.storage || "", Validators.required],
      status: [this.item?.status || "", Validators.required],
      name: [this.item?.name || "", Validators.required],
      price: [this.item?.price || "", Validators.required],
    });
  }
}
