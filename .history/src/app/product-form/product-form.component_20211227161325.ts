import { Component, OnInit } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { FormBuilder, FormGroup } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { first } from "rxjs";
import { Product } from "../product-list/product-list.component";

@Component({
  selector: "app-product-form",
  templateUrl: "./product-form.component.html",
  styleUrls: ["./product-form.component.css"],
})
export class ProductFormComponent implements OnInit {
  item?: Product;
  _myForm!: FormGroup;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private readonly afs: AngularFirestore,
    private _formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this._activatedRoute.params.subscribe((params) => {
      const { id } = params;
      if (!!id) {
        //edit
        this.afs
          .collection("Products")
          .doc(id)
          .valueChanges()
          .pipe(first())
          .subscribe((item) => {
            item = item;
            console.log("item", item);
          });
      } else {
        // add
        alert("add");
      }
    });
  }

  private initForm() {
    this._myForm = this._formBuilder.group({
      color: [this.item?.color || ""],
      storage: [this.item?.storage || ""],
      status: [this.item?.status || ""],
      name: [this.item?.name || ""],
      price: [this.item?.price || ""],
    });
  }
}
