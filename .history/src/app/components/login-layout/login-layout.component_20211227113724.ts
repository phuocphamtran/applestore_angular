import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Router } from "@angular/router";
import { AuthService } from "src/app/services/auth.service";

@Component({
  selector: "app-login-layout",
  templateUrl: "./login-layout.component.html",
  styleUrls: ["./login-layout.component.css"],
  encapsulation: ViewEncapsulation.None,
})
export class LoginLayoutComponent implements OnInit {
  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit(): void {}
  tryGoogleLogin() {
    this.authService.signinGmail().then((res) => {
      // location.href = "/product";
      this.router.navigate(["admin/product"]);
    });
  }
}
