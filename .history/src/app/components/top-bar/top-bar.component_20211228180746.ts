import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AuthService } from "src/app/services/auth.service";
import { UserService } from "src/app/services/user.service";

@Component({
  selector: "app-top-bar",
  templateUrl: "./top-bar.component.html",
  styleUrls: ["./top-bar.component.css"],
})
export class TopBarComponent implements OnInit {
  displayName: string = "";
  constructor(
    private userService: UserService,
    private authService: AuthService,
    private router: Router
  ) {}

  get avatar() {
    return this.authService.user.photoURL();
  }

  ngOnInit(): void {
    this.userService
      .getCurrentUser()
      .then(
        (user) =>
          (this.displayName =
            user.displayName != null ? user.displayName : user.email)
      );
    // console.log(this.displayName);
  }
  logout() {
    this.authService.logout();
    this.router.navigate(["login"]);
    // location.href = "/login";
  }
}
