import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { Router } from "@angular/router";
import { UserService } from "../../services/user.service";
@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"],
})
export class RegisterComponent implements OnInit {
  User: any;
  userFrom!: FormGroup;
  constructor(
    private FormBuilder: FormBuilder,
    private Router: Router,
    private Afo: UserService
  ) {
    this.userFrom = this.FormBuilder.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required, Validators.minLength(2)]],
      confirmpassword: ["", [Validators.required, Validators.minLength(2)]],
    });
  }
  Submit() {
    // if (this.userFrom.invalid) {
    //   console.log("invalid user");
    //   return;
    // }

    this.Afo.signup(
      this.userFrom.controls["email"].value,
      this.userFrom.controls["password"].value
    ).then((res) => {
      // console.log(res);
      this.Router.navigate(["login"]);
    });
    // console.log("Register is Success!");
    // alert("hihi");
    // this.Router.navigate(["login"]);
  }
  ngOnInit(): void {}
}
