import { Component, OnInit } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { Router } from "@angular/router";
import { NotifierService } from "angular-notifier";
import { AuthService } from "src/app/services/auth.service";
import { UserService } from "../../services/user.service";
@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"],
})
export class RegisterComponent implements OnInit {
  User: any;
  userFrom!: FormGroup;
  constructor(
    private FormBuilder: FormBuilder,
    private Router: Router,
    private notifier: NotifierService,
    private Afo: UserService,
    private afs: AngularFirestore,
    private authService: AuthService
  ) {
    this.userFrom = this.FormBuilder.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required, Validators.minLength(2)]],
      confirmpassword: ["", [Validators.required, Validators.minLength(2)]],
    });
  }
  Submit() {
    // if (this.userFrom.invalid) {
    //   console.log("invalid user");
    //   return;
    // }

    this.Afo.signup(
      this.userFrom.controls["email"].value,
      this.userFrom.controls["password"].value
    ).then((loggedResult) => {
      // console.log(res);
      const providerData = loggedResult.user.providerData[0];
      if (loggedResult.additionalUserInfo.isNewUser) {
        // create user
        const { email, displayName: fullName, photoURL, uid } = providerData;
        const data: any = {
          email: email,
          fullName: fullName,
          photoURL: photoURL,
          uid: uid,
        };
        this.afs
          .collection("Users")
          .add(data)
          .then((result) => {
            data.id = result.id;
            console.log("add result", data);
            this.setUsers(loggedResult.user, data);
          })
          .catch((err) => {
            alert("failed");
          });
      }
      this.Router.navigate(["login"]);
      this.notifier.notify("success", "REGISTER SUCCESS");
    });
    // console.log("Register is Success!");
    // alert("hihi");
    // this.Router.navigate(["login"]);
  }
  ngOnInit(): void {}
}
