import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"],
})
export class RegisterComponent implements OnInit {
  User: any;
  userFrom!: FormGroup;
  constructor(private FormBuilder: FormBuilder) {
    this.userFrom = this.FormBuilder.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required, Validators.minLength(2)]],
      confirmpassword: ["", [Validators.required, Validators.minLength(2)]],
    });
  }

  ngOnInit(): void {}
}
