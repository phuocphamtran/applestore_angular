import { Component, OnInit } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore } from "@angular/fire/firestore";
import {
  FormGroup,
  Validators,
  FormBuilder,
  ValidatorFn,
  AbstractControl,
  ValidationErrors,
} from "@angular/forms";
import { Router } from "@angular/router";
import { NotifierService } from "angular-notifier";
import { from } from "rxjs";
import { AuthService } from "src/app/services/auth.service";
import { UserService } from "../../services/user.service";
@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"],
})
export class RegisterComponent implements OnInit {
  User: any;
  userFrom!: FormGroup;
  constructor(
    private FormBuilder: FormBuilder,
    private Router: Router,
    private notifier: NotifierService,
    private Afo: UserService,
    private afs: AngularFirestore,
    private authService: AuthService,
    private afAuth: AngularFireAuth
  ) {
    this.userFrom = this.FormBuilder.group({
      email: [
        "",
        [Validators.required, Validators.email],
        this.uniqueValidator,
      ],
      password: ["", [Validators.required, Validators.minLength(2)]],
      confirmpassword: ["", [Validators.required, Validators.minLength(2)]],
    });
  }
  Submit() {
    // if (this.userFrom.invalid) {
    //   console.log("invalid user");
    //   return;
    // }

    this.Afo.signup(
      this.userFrom.controls["email"].value,
      this.userFrom.controls["password"].value
    ).then((loggedResult) => {
      // console.log(res);
      const providerData = loggedResult.user.providerData[0];
      if (loggedResult.additionalUserInfo.isNewUser) {
        // create user
        const { email, displayName: fullName, photoURL, uid } = providerData;
        const data: any = {
          email: email,
          fullName: fullName,
          photoURL: photoURL,
          uid: uid,
        };
        this.afs
          .collection("Users")
          .add(data)
          .then((result) => {
            data.id = result.id;
            console.log("add result", data);
            this.authService.setUsers(loggedResult.user, data);
            this.Router.navigate(["login"]);
            this.notifier.notify("success", "REGISTER SUCCESS");
          })
          .catch((err) => {
            alert("failed");
          });
      }
    });
    // console.log("Register is Success!");
    // alert("hihi");
    // this.Router.navigate(["login"]);
  }

  uniqueValidator(form: any): ValidatorFn | null {
    return (control: AbstractControl): ValidationErrors | null => {
      console.log("control..: ", control);
      const email = control.value;
      const b = this.afAuth
        .fetchSignInMethodsForEmail(email)
        .then(function (signInMethods) {
          if (signInMethods.length > 0) {
            return {
              unique: true,
            };
          }
          return null;
        });
      return b;
    };
  }
  ngOnInit(): void {}
}
