import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import firebase from "firebase/app";
import { Router } from "@angular/router";
import { AngularFirestore } from "@angular/fire/firestore";
@Injectable({
  providedIn: "root",
})
export class AuthService {
  fireUser: any;
  user: any;

  constructor(
    private afAuth: AngularFireAuth,
    private router: Router,
    private afs: AngularFirestore
  ) {}
  async signinGmail() {
    var provider = new firebase.auth.GoogleAuthProvider();
    return this.afAuth.signInWithPopup(provider).then((loggedResult: any) => {
      const providerData = loggedResult.user.providerData[0];
      if (loggedResult.additionalUserInfo.isNewUser) {
        // create user
        const { email, displayName: fullName, photoURL, uid } = providerData;
        const data: any = {
          email,
          fullName,
          photoURL,
          uid,
        };
        this.afs
          .collection("Users")
          .add(data)
          .then((result) => {
            data.id = result.id;
            console.log("add result", data);
            this.setUsers(loggedResult.user, data);
            console.log("ahihi", this.setUsers);
          });
      } else {
        // get user
        this.afs
          .collection("Users", (ref) =>
            ref.where("uid", "==", providerData.uid)
          )
          .valueChanges({ idFields: "id" })
          .subscribe((items) => {
            this.setUsers(loggedResult.user, items[0]);
            console.log("fetch user", items[0]);
          });
      }
      console.log("logged result", loggedResult);

      console.log(" da dang nhap thanh cong");

      // this.router.navigate(['home']);
    });
  }

  setUsers(fireUser: any, user: any) {
    this.fireUser = fireUser;
    this.user = user;
  }
  logout() {
    return new Promise<any>(async (resolve, reject) => {
      if (await this.afAuth.currentUser) {
        //if (this.fauth.auth.currentUser){

        this.afAuth.signOut();
        //this.sharing.isUserLoggedIn.next(false);
        resolve("log out");
      } else {
        reject();
      }
    });
  }
}
// async signinGmail() {
//   var provider = new firebase.auth.GoogleAuthProvider();
//   return await this.AngularFireAuth.signInWithPopup(provider).then((res) => {
//     console.log(" da dang nhap thanh cong");
//     this.Router.navigate(["home"]);
//   });
// }
