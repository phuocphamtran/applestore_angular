import { Component, OnInit } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import firebase from "firebase/app";
import { Router } from "@angular/router";
@Component({
  selector: "app-auth",
  templateUrl: "./auth.component.html",
  styleUrls: ["./auth.component.css"],
})
export class AuthComponent implements OnInit {
  constructor(private afAuth: AngularFireAuth, private router: Router) {}
  async signinGmail() {
    var provider = new firebase.auth.GoogleAuthProvider();
    return await this.afAuth.signInWithPopup(provider).then((res) => {
      console.log(" da dang nhap thanh cong");
    });
  }

  ngOnInit(): void {}
}
