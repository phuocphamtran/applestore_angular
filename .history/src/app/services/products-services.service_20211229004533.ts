import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Product } from "../product-list/product-list.component";
@Injectable({
  providedIn: "root",
})
export class ProductsServicesService {
  constructor(private http: HttpClient) {}
  getAll(): Observable<Product[]> {
    return this.http.get<Product[]>("http://localhost:8000/api/nodeserver");
  }
}
