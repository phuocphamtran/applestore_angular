import { Injectable } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from "@angular/router";
import { Observable } from "rxjs";
import { AuthService } from "../services/auth.service";
import { UserService } from "../services/user.service";

@Injectable({
  providedIn: "root",
})
export class AuthGuard implements CanActivate {
  constructor(
    private userService: UserService,
    private router: Router,
    private afs: AngularFirestore,
    private authService: AuthService
  ) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return new Promise((resolve, reject) => {
      this.userService
        .getCurrentUser() //kiem tra có user đang đăng nhập hay không nếu có trả về true, ngược lại trả về false
        .then(
          (user) => {
            console.log(user);
            this.afs
              .collection("Users", (ref) => ref.where("uid", "==", user.uid))
              .valueChanges({ idField: "id" })
              .subscribe((item) => {
                alert("ok");
                console.log("auth guard user", item);
                this.authService.setUsers(user, item);
              });

            resolve(true);
          },
          (err) => {
            resolve(false);
            this.router.navigate(["/login"]);
          }
        );
    });

    // return true;
  }
}
