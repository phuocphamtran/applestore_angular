import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Observable } from "rxjs";
import { Product } from "../product-list/product-list.component";
import { ProductsServicesService } from "../services/products-services.service";

@Component({
  selector: "app-node-product",
  templateUrl: "./node-product.component.html",
  styleUrls: ["./node-product.component.css"],
})
export class NodeProductComponent implements OnInit {
  productList: Observable<Product[]>;
  products: Product[] = [];
  config: any;
  constructor(
    private service: ProductsServicesService,
    private router: Router
  ) {
    this.productList = this.service.getAll();
    this.productList.subscribe((data) => {
      this.products = data;
      this.config = {
        itemsPerPage: 3,
        currentPage: 1,
        totalItems: this.products.length,
      };
    });
  }
  pageChanged(event: number) {
    this.config.currentPage = event;
  }

  ngOnInit(): void {}
}
