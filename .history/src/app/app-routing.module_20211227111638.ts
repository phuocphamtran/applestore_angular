import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginLayoutComponent } from "./components/login-layout/login-layout.component";
import { LoginComponent } from "./components/login/login.component";
import { MainLayoutComponent } from "./components/main-layout/main-layout.component";
import { ProductListComponent } from "./product-list/product-list.component";

const routes: Routes = [
  { path: "login", component: LoginLayoutComponent },
  { path: "admin", component: MainLayoutComponent,  children:[
				{path:"insert", component:InsertItemComponent},          
				{path:'productlist', component:ProductListComponent}           
			  ]}, },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
