import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { FooterComponent } from "./components/footer/footer.component";
import { SidebarComponent } from "./components/sidebar/sidebar.component";
import { TopBarComponent } from "./components/top-bar/top-bar.component";
import { ProductListComponent } from "./product-list/product-list.component";

import { AngularFireModule } from "@angular/fire";
import { AngularFirestoreModule } from "@angular/fire/firestore";
import { environment } from "../environments/environment";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { NgxPaginationModule } from "ngx-pagination";

import { LoginComponent } from "./components/login/login.component";
import { LoginLayoutComponent } from "./components/login-layout/login-layout.component";
import { MainLayoutComponent } from "./components/main-layout/main-layout.component";
import { InsertNdodeComponent } from "./components/insert-ndode/insert-ndode.component";
import { ProductFormComponent } from "./product-form/product-form.component";
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    SidebarComponent,
    TopBarComponent,

    ProductListComponent,

    LoginComponent,
    LoginLayoutComponent,
    MainLayoutComponent,
    InsertNdodeComponent,
    ProductFormComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    NgxPaginationModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
