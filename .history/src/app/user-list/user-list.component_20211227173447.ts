import { Component, OnInit } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore } from "@angular/fire/firestore";
import { Observable } from "rxjs";
export interface User {
  id?: string;
  userName?: string;
  email?: string;
  team?: number;
  status?: string;
  created?: number;
}
@Component({
  selector: "app-user-list",
  templateUrl: "./user-list.component.html",
  styleUrls: ["./user-list.component.css"],
})
export class UserListComponent implements OnInit {
  constructor(
    private fireAuth: AngularFireAuth,
    private readonly afs: AngularFirestore
  ) {}

  ngOnInit(): void {}
}
