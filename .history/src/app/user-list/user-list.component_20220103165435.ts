import { Component, OnInit } from "@angular/core";
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from "@angular/fire/firestore";
import { NotifierService } from "angular-notifier";
import { Observable } from "rxjs";

export interface User {
  email?: string;
  fullName?: string;
  // photoURL?: string;
  role?: string;
  uid?: string;
  status?: string;
}

@Component({
  selector: "app-user-list",
  templateUrl: "./user-list.component.html",
  styleUrls: ["./user-list.component.css"],
})
export class UserListComponent implements OnInit {
  private usersCollection: AngularFirestoreCollection<User>;
  users: Observable<User[]>;
  users1: User[] = [];
  config: any;
  constructor(
    private readonly afs: AngularFirestore,
    private notifier: NotifierService
  ) {
    this.usersCollection = afs.collection<User>("Users");
    this.users = this.usersCollection.valueChanges({ idField: "uid" });
    this.users.subscribe((data) => {
      this.users1 = data;
      console.log(this.users1);
      this.config = {
        itemsPerPage: 2,
        currentPage: 1,
        totalItems: this.users1.length,
      };
      console.log("users1 varaiable: ", this.users1);
    });
  }

  pageChanged(event: number) {
    this.config.currentPage = event;
  }
  ngOnInit(): void {}
  public deleteItem(id: string) {
    const r = confirm("Do you want to delete?");
    if (r) {
      this.afs
        .collection("Users")
        .doc(id)
        .delete()
        .then((_) => {
          this.notifier.notify("success", "Delete success");
        });
    }
  }
}
