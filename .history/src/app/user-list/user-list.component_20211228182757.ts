import { Component, OnInit } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from "@angular/fire/firestore";
import { Router } from "@angular/router";
import { Observable } from "rxjs";
import { AuthService } from "../services/auth.service";
import { UserService } from "../services/user.service";

export interface User {
  email?: string;
  fullName?: string;
  password?: string;
  role?: number;
  uid?: string;
  username?: number;
}

@Component({
  selector: "app-user-list",
  templateUrl: "./user-list.component.html",
  styleUrls: ["./user-list.component.css"],
})
export class UserListComponent implements OnInit {
  private usersCollection: AngularFirestoreCollection<User>;
  users: Observable<User[]>;
  users1: User[] = [];
  config: any;
  constructor(private readonly afs: AngularFirestore) {
    this.usersCollection = afs.collection<User>("Users");
    this.users = this.productsCollection.valueChanges({ idField: "uid" });
    this.users.subscribe((data) => {
      this.users1 = data;
      this.config = {
        itemsPerPage: 2,
        currentPage: 1,
        totalItems: this.users1.length,
      };
      console.log("users1 varaiable: ", this.users1);
    });
    // this.add("9", "PhuocTest", "Silver", 256, "New", 12860000);
    // this.update("3", "10", "PhuocTest", "Silver", 256, "New", 28860000);
  }

  pageChanged(event: number) {
    this.config.currentPage = event;
  }
  ngOnInit(): void {}
  public deleteItem(id: string) {
    const r = confirm("Do you want to delete?");
    if (r) {
      this.afs
        .collection("Products")
        .doc(id)
        .delete()
        .then((_) => {
          alert("delete success");
        });
    }
  }
}
