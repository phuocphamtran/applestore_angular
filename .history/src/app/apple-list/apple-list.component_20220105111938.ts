import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { NotifierService } from "angular-notifier";
import { Observable } from "rxjs";
import { Product } from "../product-list/product-list.component";
import { ProductsServicesService } from "../services/products-services.service";

@Component({
  selector: "app-apple-list",
  templateUrl: "./apple-list.component.html",
  styleUrls: ["./apple-list.component.css"],
})
export class AppleListComponent implements OnInit {
  productList: Observable<Product[]>;
  products: Product[] = [];
  config: any;
  constructor(
    private service: ProductsServicesService,
    private router: Router,
    private notifier: NotifierService
  ) {
    this.productList = this.service.getAll();
    this.productList.subscribe((data) => {
      this.products = data;
      console.log(data);
      this.config = {
        itemsPerPage: 4,
        currentPage: 1,
        totalItems: this.products.length,
      };
    });
  }
  pageChanged(event: number) {
    this.config.currentPage = event;
  }

  delete(id?: string) {
    const notiDelete = confirm("Do you want to delete?");
    if (notiDelete) {
      this.service.delete(id).subscribe((res: any) => {
        this.notifier.notify("success", "Delete success");
        window.location.reload();
      });
    }
  }

  ngOnInit(): void {}
}
