import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginLayoutComponent } from "./components/login-layout/login-layout.component";
import { LoginComponent } from "./components/login/login.component";
import { MainLayoutComponent } from "./components/main-layout/main-layout.component";
import { ProductListComponent } from "./product-list/product-list.component";

const routes: Routes = [
  { path: "", component: LoginLayoutComponent },
  { path: "login", component: LoginComponent },
  {
    path: "admin",
    component: MainLayoutComponent,
    canActivate: [AuthGuardGuard],
    children: [{ path: "product", component: ProductListComponent }],
  },
  { path: "**", component: LoginLayoutComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
