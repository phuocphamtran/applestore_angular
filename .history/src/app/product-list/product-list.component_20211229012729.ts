import { Component, OnInit } from "@angular/core";
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from "@angular/fire/firestore";
import { first, Observable } from "rxjs";

export interface Product {
  id?: string;
  name?: string;
  color?: string;
  storage?: number;
  status?: string;
  price?: number;
}

@Component({
  selector: "app-product-list",
  templateUrl: "./product-list.component.html",
  styleUrls: ["./product-list.component.css"],
})
export class ProductListComponent implements OnInit {
  searchText: string = "";
  private productsCollection: AngularFirestoreCollection<Product>;
  products: Observable<Product[]>;
  products1: Product[] = [];
  config: any;
  searchingProducts: any[] = [];
  searchVal = "";

  constructor(private readonly afs: AngularFirestore) {
    this.productsCollection = afs.collection<Product>("Products");
    this.products = this.productsCollection.valueChanges({ idField: "id" });
    this.products.subscribe((data) => {
      this.products1 = data;
      this.config = {
        itemsPerPage: 2,
        currentPage: 1,
        totalItems: this.products1.length,
      };
      console.log("products1 varaiable: ", this.products1);
    });
    // this.add("9", "PhuocTest", "Silver", 256, "New", 12860000);
    // this.update("3", "10", "PhuocTest", "Silver", 256, "New", 28860000);
    // ref
    //   .where(field, ">=", value)
    //   .where(field, "<=", value + "\uf8ff")
    //   .orderBy(field, "desc");
  }

  pageChanged(event: number) {
    this.config.currentPage = event;
  }

  ngOnInit(): void {}
  public deleteItem(id: string) {
    const r = confirm("Do you want to delete?");
    if (r) {
      this.afs
        .collection("Products")
        .doc(id)
        .delete()
        .then((_) => {
          alert("delete success");
        });
    }
  }

  public onSearch($event: any) {
    this.searchVal = $event.target.value;
    if (this.searchVal.trim() != "") {
      this.afs
        .collection("Products", (ref) =>
          ref
            .where("name", ">=", this.searchVal)
            .where("name", "<=", this.searchVal + "\uf8ff")
        )
        .valueChanges({ idField: "id" })
        .pipe(first())
        .subscribe((items) => {
          console.log("searching items", items);
          this.searchingProducts = items;
        });
    }

    // ref
    //   .where(field, ">=", value)
    //   .where(field, "<=", value + "\uf8ff")
    //   .orderBy(field, "desc");
  }

  get list() {
    if (this.searchVal != "") return this.searchingProducts;
    return this.products1;
  }
}
