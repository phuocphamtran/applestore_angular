import { Component, OnInit } from "@angular/core";
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from "@angular/fire/firestore";
import { Observable } from "rxjs";

export interface Product {
  id?: string;
  name?: string;
  color?: string;
  storage?: number;
  status?: string;
  price?: number;
}

@Component({
  selector: "app-product-list",
  templateUrl: "./product-list.component.html",
  styleUrls: ["./product-list.component.css"],
})
export class ProductListComponent implements OnInit {
  private productsCollection: AngularFirestoreCollection<Product>;
  products: Observable<Product[]>;
  products1: Product[] = [];
  config: any;
  constructor(private readonly afs: AngularFirestore) {
    this.productsCollection = afs.collection<Product>("Products");
    this.products = this.productsCollection.valueChanges({ idField: "key" });
    this.products.subscribe((data) => {
      this.products1 = data;
      this.config = {
        itemsPerPage: 2,
        currentPage: 1,
        totalItems: this.products1.length
      };
        console.log("products1 varaiable: ", this.products1);
      });
      // this.add("9", "PhuocTest", "Silver", 256, "New", 12860000);
      // this.update("3", "10", "PhuocTest", "Silver", 256, "New", 28860000);
    }
  pageChanged(event: number) {
      this.config.currentPage = event;
    }
  ngOnInit(): void {}
  

  