import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginLayoutComponent } from "./components/login-layout/login-layout.component";
import { LoginComponent } from "./components/login/login.component";
import { MainLayoutComponent } from "./components/main-layout/main-layout.component";
import { AuthGuard } from "./guards/auth.guard";
import { ProductFormComponent } from "./product-form/product-form.component";
import { ProductListComponent } from "./product-list/product-list.component";

const routes: Routes = [
  { path: "", component: LoginLayoutComponent },
  { path: "login", component: LoginComponent },
  {
    path: "admin",
    component: MainLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      { path: "product", component: ProductListComponent },
      { path: "product/form/:id", component: ProductFormComponent },
      { path: "product/form", component: ProductFormComponent },
    ],
  },
  { path: "**", component: LoginLayoutComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
