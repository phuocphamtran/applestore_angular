import { Component, OnInit } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { first } from "rxjs";

@Component({
  selector: "app-user-form",
  templateUrl: "./user-form.component.html",
  styleUrls: ["./user-form.component.css"],
})
export class UserFormComponent implements OnInit {
  item?: any;
  _myForm!: FormGroup;
  constructor(
    private _activatedRoute: ActivatedRoute,
    private afs: AngularFirestore,
    private _formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this._activatedRoute.params.subscribe((params) => {
      const { id } = params;
      if (!!id) {
        //edit
        this.afs
          .collection("Users")
          .doc(id)
          .valueChanges({ idField: "id" })
          .pipe(first())
          .subscribe((item) => {
            this.item = item;
            this.initForm();
            console.log("item", item);
          });
      } else {
        // add
        this.initForm();
      }
    });
  }

  public onSubmit() {
    if (this._myForm.valid) {
      const updateUser = this._myForm.value;
      if (!!this.item?.id) {
        this.afs
          .collection("Products")
          .doc(this.item!.id)
          .update(updateUser)
          .then((_) => {
            alert("update success");
          })
          .catch((_) => {
            alert("ERRR");
          });
      } else {
        this.afs
          .collection("Users")
          .add(updateUser)
          .then((_) => {
            this._myForm.reset();
          });
      }
    }
  }

  public isSubmitDisabled() {
    return !this._myForm.valid;
  }

  private initForm() {
    // alert("init" + this.item?.id);
    this._myForm = this._formBuilder.group({
      email: [this.item?.email || "", Validators.required],
      fullName: [this.item?.fullName || "", Validators.required],
      role: [this.item?.role || "", Validators.required],
    });
  }
}
