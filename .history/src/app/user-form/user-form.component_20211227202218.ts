import { Component, OnInit } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { FormBuilder, FormGroup } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { first } from "rxjs";

@Component({
  selector: "app-user-form",
  templateUrl: "./user-form.component.html",
  styleUrls: ["./user-form.component.css"],
})
export class UserFormComponent implements OnInit {
  item?: any;
  _myForm!: FormGroup;
  constructor(
    private _activatedRoute: ActivatedRoute,
    private afs: AngularFirestore,
    private _formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {}
}
